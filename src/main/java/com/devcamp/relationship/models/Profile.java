package com.devcamp.relationship.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="profiles")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @Column(name="phone")
    public String phone;
    @Column(name="gender")
    public String gender;
    @Temporal(TemporalType.DATE)
    @Column(name="dateofbirth")
    public Date dateOfBirth;
    @Column(name="address")
    public String address;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable = true)
    public User user;
}
